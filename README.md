# screenshot tools

Intended to be a faster `spectacle` alternative.


## Dependencies (other than Cargo etc)

- maim
- xclip

## Shortcuts

- Close with `q`
- Take a screenshot with `s`
- Save file with `w`
- Copy to clipboard with `y`

## TODO

- [ ] draggable image
- [x] copy to clipboard
- [ ] delayed screenshots
