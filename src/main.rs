use chrono::prelude::*;
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Mutex};

use gdk::EventKey;
use gdk_pixbuf::{InterpType, Pixbuf};
use gio::{prelude::*, Cancellable, MemoryInputStream};
use glib::Bytes;
use gtk::{
    prelude::*, Application, ApplicationWindow, Box, EventBox, Image, Orientation, Statusbar,
};
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short = "d", long, parse(from_os_str), default_value = ".")]
    dir: std::path::PathBuf,
}

pub struct Screenshot {
    png: Vec<u8>,
    timestamp: DateTime<Local>,
    saved: usize,
}

pub type ScreenshotRef = Arc<Mutex<Option<Screenshot>>>;

pub enum Mode {
    Full,
    Select,
}

#[derive(Clone)]
struct App {
    window: ApplicationWindow,
    layout: Box,
    image_box: EventBox,
    image: Image,
    status: Statusbar,
    context_id: u32,
    screenshot_ref: ScreenshotRef,
    screenshot_dir: std::path::PathBuf,
}

impl App {
    pub fn new(application: &Application) -> Self {
        let opt = Opt::from_args();
        let screenshot_ref: ScreenshotRef = Arc::new(Mutex::new(None));

        // initialise widgets and configure some properties
        let window = ApplicationWindow::new(application);
        window.set_title("screenshot");
        window.set_default_size(350, 70);
        let layout = Box::new(Orientation::Vertical, 10);
        window.add(&layout);
        let image_box = EventBox::new();
        layout.add(&image_box);
        let image = Image::new_from_pixbuf(None);
        image_box.add(&image);
        let status = gtk::Statusbar::new();
        layout.add(&status);
        let context_id = status.get_context_id("default");

        let self_ = Self {
            window,
            layout,
            image_box,
            image,
            status,
            context_id,
            screenshot_ref,
            screenshot_dir: opt.dir,
        };

        self_.connect();
        self_.take_screenshot(Mode::Select);
        self_.show_screenshot();
        self_
    }

    // define behaviour
    fn connect(&self) {
        let self_ = self.clone();
        self.window
            .connect_key_press_event(move |_, ev: &EventKey| {
                match ev.get_keyval() {
                    // q
                    113 => self_.window.close(),
                    // s
                    115 => {
                        self_.take_screenshot(Mode::Select);
                        self_.show_screenshot();
                    }
                    // y
                    121 => {
                        self_.to_clipboard();
                    }
                    // w
                    119 => {
                        self_.save_screenshot();
                    }
                    _ => {}
                }
                gtk::Inhibit(true)
            });
    }

    fn status_message(&self, msg: &str) {
        self.status.push(self.context_id, &msg);
    }

    pub fn show_all(&self) {
        self.window.show_all();
    }

    fn show_screenshot(&self) {
        if let Some(mut pixbuf) = self.to_pixbuf() {
            let w = pixbuf.get_width();
            let h = pixbuf.get_height();
            if h == 0 {
                return;
            };
            let r = (w as f64) / (h as f64);
            if w > 600 || h > 400 {
                let (nw, nh) = if r > 600.0 / 400.0 {
                    (600, (600.0 / r).floor() as i32)
                } else {
                    ((400.0 * r).floor() as i32, 400)
                };
                pixbuf = pixbuf
                    .scale_simple(nw, nh, InterpType::Hyper)
                    .expect("unable to scale image");
            };
            self.image.set_from_pixbuf(Some(&pixbuf));
        } else {
            self.image.set_from_pixbuf(None);
        }
    }

    fn take_screenshot(&self, mode: Mode) {
        let png = match mode {
            Mode::Full => std::process::Command::new("maim")
                .output()
                .map(|o| o.stdout)
                .ok(),
            Mode::Select => std::process::Command::new("maim")
                .arg("-u")
                .arg("-b")
                .arg("2")
                .arg("-c")
                .arg("1,1,1,1")
                .arg("-r")
                .arg("invert")
                .arg("-s")
                .output()
                .map(|o| o.stdout)
                .ok(),
        };
        if let Ok(mut screenshot) = self.screenshot_ref.lock() {
            println!(
                "Screenshot size: {}",
                png.iter().map(|o| o.len()).sum::<usize>()
            );

            *screenshot = png.map(|png| Screenshot {
                png,
                timestamp: Local::now(),
                saved: 0,
            });
            self.status_message("Screenshot taken");
        } else {
            self.status_message("Error: could not take screenshot");
        }
    }

    fn save_screenshot(&self) {
        if let Some(screenshot) = &mut self.screenshot_ref.lock().unwrap().deref_mut() {
            // in case the saved file has been deleted in the meanwhile,
            // you can press save 10 times to redo the saving, even if the file has been
            // saved once already.
            if screenshot.saved == 0 || screenshot.saved > 10 {
                // find suitable file for saving...
                let destination = {
                    let screenshot_dir = self.screenshot_dir.clone();
                    if !screenshot_dir.exists() && !screenshot_dir.is_dir() {
                        self.status_message(&format!(
                            "Screenshot directory {:?} is not a directory",
                            &screenshot_dir
                        ));
                        return;
                    } else if !screenshot_dir.exists()
                        && std::fs::create_dir(&screenshot_dir).is_err()
                    {
                        self.status_message("Error: Unable to create screenshot dir.");
                        return;
                    }
                    let basename = format!(
                        "Screenshot_{}",
                        screenshot.timestamp.format("%Y%m%d_%H%M%S")
                    );
                    let path = screenshot_dir.join(&basename).with_extension("png");
                    if path.exists() {
                        if let Some(path) = (0..100)
                            .map(|i| {
                                screenshot_dir
                                    .join(format!("{}-{:02}", &basename, i))
                                    .with_extension("png")
                            })
                            .find(|path| !path.exists())
                        {
                            path
                        } else {
                            self.status_message("Error: Unable to save. gomennasai");
                            return;
                        }
                    } else {
                        path
                    }
                };

                std::fs::write(&destination, &screenshot.png).unwrap();
                screenshot.saved = 1;
                self.status_message(&format!("Screenshot saved as {:?}", &destination));
            };
            screenshot.saved += 1;
        }
    }

    fn to_clipboard(&self) {
        if let Some(screenshot) = &self.screenshot_ref.lock().unwrap().deref() {
            if let Ok(mut child) = std::process::Command::new("xclip")
                .arg("-selection")
                .arg("clipboard")
                .arg("-t")
                .arg("image/png")
                .stdin(std::process::Stdio::piped())
                .spawn()
            {
                use std::io::Write;
                child
                    .stdin
                    .as_mut()
                    .unwrap()
                    .write_all(&screenshot.png)
                    .unwrap();
                child.wait_with_output().unwrap();
                self.status_message("Copied to clipboard.");
            } else {
                self.status_message("Error: Unable to copy to clipboard.");
            }
        }
    }

    fn to_pixbuf(&self) -> Option<Pixbuf> {
        let bytes = self.to_bytes()?;
        let glib_bytes = Bytes::from(&bytes);
        let stream = MemoryInputStream::new_from_bytes(&glib_bytes);
        Pixbuf::new_from_stream(&stream, None as Option<&Cancellable>).ok()
    }

    fn to_bytes(&self) -> Option<Vec<u8>> {
        Some(
            ((self.screenshot_ref.as_ref()).lock().unwrap())
                .as_ref()
                .unwrap()
                .png
                .clone(),
        )
    }
}

fn main() {
    let application =
        Application::new(Some("de.huynhgia.putputlaw.screenshot"), Default::default())
            .expect("failed to initialize GTK application");

    application.connect_activate(|app| {
        let screenshot_app = App::new(&app);

        screenshot_app.show_all();
    });

    application.run(&[]);
}
